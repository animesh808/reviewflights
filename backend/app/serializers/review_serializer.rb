class ReviewSerializer
  include FastJsonapi::ObjectSerializer
  attributes :title, :description, :airline_id, :score
end
