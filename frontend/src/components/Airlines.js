import React, {useState, useEffect} from 'react';
import AirlineCard from './AirlineCard';
import axios from 'axios';

const Airlines = () => {
  const [airlines, setAirlines] = useState([])

  useEffect(()=>{
    axios.get('http://localhost:3001/api/v1/airlines')
    .then(resp => setAirlines(resp.data.data))
    .catch(resp => console.log(resp))
  },[airlines.length])

  const grid = airlines.map( (item,index) => {
    return(
      <div className="col-md-3" style={{marginTop: '15px'}} key={item.attributes.name}>
        <AirlineCard attributes={item.attributes}></AirlineCard>
      </div>
    )
  })

  return(
    <div className="jumbotron">
      <div className="text-center">
        <p className="display-3">OpenFlights</p>
        <p className="lead">Honest, unbaised airlines review</p>
        <hr/>
      </div>
      <div className="container-fluid">
        <div className="row">{grid}</div>
      </div>
    </div>
  );
}

export default Airlines;
