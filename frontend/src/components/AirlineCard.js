import React from 'react';
import {Link} from 'react-router-dom';
import {Card,Button} from 'react-bootstrap';

const AirlineCard = (props) => {
  return(
    <div>
      <Card style={{ width: '', backgroundColor: '#d1cbcb' }} key={props.attributes.id}>
        <div className="text-center">
          <img
            src={props.attributes.image_url}
            alt={props.attributes.name}
            className="rounded-circle img-fluid"
            style={{marginTop: '15px',height: '150px',width: '150px'}}
          />
        </div>
        <Card.Body>
          <Card.Title className="text-center">{props.attributes.name}</Card.Title>
          <Card.Text>
            Average score of this airline is <b className="text-info display-4">{props.attributes.avg_score}</b>
          </Card.Text>
          <Button variant="dark"><Link className="text-white" to={"/airlines/" + props.attributes.slug}>View Airline</Link></Button>
        </Card.Body>
      </Card>
    </div>
  );
}

export default AirlineCard;
