import React from 'react';
import {Card,ListGroup,ListGroupItem } from 'react-bootstrap';

const AirlineDetails = (props) =>{
  return(
    <div>
      <h2 style={{padding: '10px 0px 10px 20px', marginRight: '-15px'}} className="text-dark">Airline Details & Reviews</h2>
      <div className="row" style={{padding: '20px 0 20px 20px'}}>
        <div className="col-md-4">
          <img className='rounded-circle img-fluid' src={props.attributes.image_url} alt={props.attributes.name} height="200px" width="200px" />
        </div>
        <div className="col-md-6" style={{paddingTop: '5%'}}>
          <h2>{props.attributes.name}</h2>
          <p><b>Ratings:</b> <span className="text-danger font-weight-bold">{props.attributes.avg_score}</span> out of <b>5</b></p>
        </div>
      </div><br/>
      <h4 style={{paddingLeft: '3%'}}>Reviews ({props.reviews.length}):</h4>
      <Card style={{ width: '' }}>
        <ListGroup className="list-group-flush">
          {props.reviews.length < 1 &&
            <ListGroupItem>
              <h5>No reviews available</h5>
            </ListGroupItem>
          }
          {props.reviews.map((review,index) =>{
            return(
              <ListGroupItem key={index}>
                <div className="row">
                  <div className="col-md-10">
                    <h5>{review.attributes.title}</h5>
                    <span>{review.attributes.description}</span>
                  </div>
                  <div className="col-md-2 text-center text-light bg-dark">
                    <h4>Rating</h4>
                    <span className="font-weight-bold">{review.attributes.score}</span>
                  </div>
                </div>
              </ListGroupItem>
            )
          })}
        </ListGroup>
      </Card>
    </div>
  )
}

export default AirlineDetails;
