import React, { useCallback,useState, useEffect } from 'react';
import axios from 'axios';
import AirlineDetails from './AirlineDetails';
import ReviewForm from './ReviewForm';

const Airline = (props) => {
  const [airline, setAirline] = useState({})
  const [review, setReview] = useState({})
  const [loaded, setLoaded] = useState(false)
  const slug = props.match.params.slug
  const url = `http://localhost:3001/api/v1/airlines/${slug}`

  const fetchAirline = useCallback(() => {
    axios.get(url)
    .then(resp => {
      setAirline(resp.data)
      setLoaded(true)
    })
    .catch( data => console.log('Error', data) )
  }, [url])

  useEffect(() => {
    fetchAirline(url)
  },[props,fetchAirline,url])


  const handleSubmit = (e) => {
    e.preventDefault();
    const airline_id = airline.data.id
    axios.post('http://localhost:3001/api/v1/reviews/', {review, airline_id})
    .then(resp =>{
      const included = [...airline.included, resp.data.data]
      setAirline({...airline, included})
      setReview({title: '', description: '', score: 0})
      fetchAirline(url)
    })
    .catch(resp => {})
    document.getElementById('reviewForm').reset()
  }

  const handleChange = (e) =>{
    e.preventDefault();
    setReview({...review, [e.target.name]: e.target.value})
  }

  const handleRating = (rate,e) =>{
    setReview({...review, score: rate})
  }

  return(
    <div>
      {loaded &&
        <div className="row" style={{height: '100vh'}}>
          <div className="col-md-6">
            <AirlineDetails attributes={airline.data.attributes} reviews={airline.included} />
          </div>
          <div className="col-md-6" style={{backgroundColor: 'black'}}>
            <ReviewForm
              handleChange={handleChange}
              handleRating={handleRating}
              handleSubmit={handleSubmit}
              airline={airline.data.attributes}
              review={review}
            />
          </div>
        </div>
      }
    </div>
  )
}

export default Airline
