import React from 'react';
import Rating from 'react-rating';
const ReviewForm = (props) => {
  return(
    <div>
      <h4 style={{padding: '10px 0px 10px 20px', marginLeft: '-15px'}} className="text-white">Have an experience with {props.airline.name}. Share your review.</h4>
      <form onSubmit={props.handleSubmit} id='reviewForm'>
        <div className="form-group col-md-12">
          <label htmlFor="title" className="text-light">Review Title</label>
          <input type="text" className="form-control" onChange={props.handleChange} value={props.title} name="title" id="title" placeholder="Your review title" />
        </div>

        <div className="form-group col-md-12">
          <label className="text-light" htmlFor="description">Review Description</label>
          <textarea className="form-control" onChange={props.handleChange} value={props.description} name="description" id="description" rows="3"></textarea>
        </div>

        <div className="form-group col-md-12">
          <label className="text-light" htmlFor="rating">Your Rating: </label><br/>
          <Rating
            onClick={(rate) => props.handleRating(rate)}
            initialRating={props.review.score}
          />
        </div>

        <div className="col-md-12 text-right">
          <button type="submit" className="btn btn-info btn-lg">Submit</button>
        </div>
      </form>
    </div>
  )
}

export default ReviewForm;
