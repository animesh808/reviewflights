import React from 'react';
import './App.css';
import {Switch, Route} from 'react-router-dom';
import AirlineShow from './components/AirlineShow';
import Airlines from './components/Airlines';

const App = () => {

  return(
    <Switch>
      <Route exact path="/" component={Airlines} />
      <Route exact path="/airlines/:slug" component={AirlineShow} />
    </Switch>
  );

}

export default App;
